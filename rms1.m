function y = rms1 (x)
  y = sqrt(sum(x.^2)/length(x));
