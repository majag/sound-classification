[file1, fs] = audioread('music_speech/music_wav/ballad.wav');
[file2, fs2] = audioread('music_speech/speech_wav/voices.wav');
%sound(file1,fs)
%sound(file2,fs)

t = [1:length(file1)]/fs;
t2 = [1:length(file2)]/fs2;

figure;
subplot(1,2,1)
plot(t, file1)
title('Music')
xlabel('time (s)')
ylabel('frequency')
subplot(1,2,2)
plot(t2, file2)
title('Speech')
xlabel('time (s)')
ylabel('frequency')


