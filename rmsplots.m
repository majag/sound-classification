% signal glazbe

[x, fs] = audioread('music_speech/music_wav/ballad.wav');

sigLength = length(x);		%661500
velicinaMarkera = 3;
subplot(1,2,1); 
axis([0 sigLength 0 0.1]); 
hold on;
 
x1 = x(1:66150);
rmsV = rms1(x1);
hlines = plot(1:66150,rmsV,'r.','MarkerSize',velicinaMarkera);

xlabel('Signal'); 
ylabel('RMS');
title('RMS vrijednosti signala glazbe ballad.wav', 'FontWeight', 'bold');
 
x1 = x(66150:132300);
rmsV = rms1(x1);
plot(66150:132300,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x(132300:198450);
rmsV = rms1(x1);
plot(132300:198450,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x(198450:264600);
rmsV = rms1(x1);
plot(198450:264600,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x(264600:330750);
rmsV = rms1(x1);
plot(264600:330750,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x(330750:396900);
rmsV = rms1(x1);
plot(330750:396900,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x(396900:463050);
rmsV = rms1(x1);
plot(396900:463050,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x(463050:529200);
rmsV = rms1(x1);
plot(463050:529200,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x(529200:595350);
rmsV = rms1(x1);
plot(529200:595350,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x(595350:sigLength);
rmsV = rms1(x1);
plot(595350:sigLength,rmsV,'r.','MarkerSize',velicinaMarkera);

hold off;

% signal govora

[x2, fx2] = audioread('music_speech/speech_wav/voices.wav');

sigLength2 = length(x2);		%661500
subplot(1,2,2); 
axis([0 sigLength2 0 0.1]); 
hold on;
 
x1 = x2(1:66150);
rmsV = rms1(x1);
hlines = plot(1:66150,rmsV,'r.','MarkerSize',velicinaMarkera);

xlabel('Signal'); 
ylabel('RMS');
title('RMS vrijednosti signala govora voices.wav', 'FontWeight', 'bold');
 
x1 = x2(66150:132300);
rmsV = rms1(x1);
plot(66150:132300,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x2(132300:198450);
rmsV = rms1(x1);
plot(132300:198450,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x2(198450:264600);
rmsV = rms1(x1);
plot(198450:264600,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x2(264600:330750);
rmsV = rms1(x1);
plot(264600:330750,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x2(330750:396900);
rmsV = rms1(x1);
plot(330750:396900,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x2(396900:463050);
rmsV = rms1(x1);
plot(396900:463050,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x2(463050:529200);
rmsV = rms1(x1);
plot(463050:529200,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x2(529200:595350);
rmsV = rms1(x1);
plot(529200:595350,rmsV,'r.','MarkerSize',velicinaMarkera); 

x1 = x2(595350:sigLength2);
rmsV = rms1(x1);
plot(595350:sigLength2,rmsV,'r.','MarkerSize',velicinaMarkera);

hold off;
