function y = rms(x, dim)
%RMS    Root mean squared value.
%   For vectors, RMS(X) is the root mean squared value in X. For matrices,
%   RMS(X) is a row vector containing the RMS value from each column. For
%   N-D arrays, RMS(X) operates along the first non-singleton dimension.
%
%   Y = RMS(X,DIM) operates along the dimension DIM.
%
%   When X is complex, the RMS is computed using the magnitude
%   RMS(ABS(X)). 
%
%   % Example #1: RMS of sinusoid vector 
%   x = cos(2*pi*(1:100)/100);
%   y = rms(x)
%
%   % Example #2: RMS of columns of matrix
%   x = [rand(100000,1) randn(100000,1)]; 
%   y = rms(x, 1)  
%
%   % Example #3: RMS of rows of matrix
%   x = [2 -2 2; 3 3 -3]; 
%   y = rms(x, 2)  
%
%   See also MIN, MAX, MEDIAN, MEAN, STD, PEAK2RMS.

%   Copyright 2011 The MathWorks, Inc.

% modificirana originalna MATLAB funkcija 
% vra�a postotak okvira niske energije u uzorku
% svaki uzorak se dijeli na okvire duzine 0.050 tj 50 milisec
% �to zna�i 20 okvira po sekundi

[x, fs] = audioread(x);

file_length = length(x);

% okviri

frame_size = 0.050;
frame_length = round(fs*frame_size);        
frames_per_sec = round(1/frame_size);

% if nargin==1
%   y = sqrt(mean(x .* conj(x)));
% else
%   y = sqrt(mean(x .* conj(x), dim));
% end

% racunanje RMS vrijednosti svakog okvira

y = [];
n = 1;
low = 0;

for i = 1:frame_length:file_length-frame_length
	frameData = x(i:i+frame_length-1);

	% vrijednosti RMS jednog okvira
	y(n) = sqrt(sum(frameData.^2)/length(frameData));
	n = n + 1;
end
num_frames = length(y);

% zbroji sve low energy okvire
low = 0;
if (num_frames>frames_per_sec)
	for j = frames_per_sec+1:num_frames
		meanY(j) = mean(y(j-frames_per_sec:j));
		if(y(j) < 0.5*meanY(j))
			low = low + 1;
		end
	end
end

% rezultat je postotak okvira niske energije
y = low/(num_frames-frames_per_sec);

