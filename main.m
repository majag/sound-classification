% Klasifikacija zvuka na glazbu ili govor

% U�itavanje i priprema podataka

music_directory = dir('music_speech/music_wav/*.wav');
music_directory = struct2cell(music_directory);
music_directory = music_directory(1,:);
speech_directory = dir('music_speech/speech_wav/*.wav');
speech_directory = struct2cell(speech_directory);
speech_directory = speech_directory(1,:);

disp(['Omjer podataka (u�enje:testiranje). Odaberite: ']);
disp(['1 za omjer 80:20']);
disp(['2 za omjer 70:30']);
disp(['3 za omjer 60:40']);
disp(['4 za omjer 50:50']);


odgovor = input('Unesite: ');

if (odgovor == 1)
    train_music_files = music_directory(:, 1:51);       
    music_files = music_directory(:, 52:end);           

    train_speech_files = speech_directory(:, 1:51);     
    speech_files = speech_directory(:, 52:end); 
elseif (odgovor == 2)
    train_music_files = music_directory(:, 1:45);       
    music_files = music_directory(:, 46:end);           

    train_speech_files = speech_directory(:, 1:45);      
    speech_files = speech_directory(:, 46:end);      
elseif (odgovor == 3)
    train_music_files = music_directory(:, 1:38);       
    music_files = music_directory(:, 39:end);           

    train_speech_files = speech_directory(:, 1:38);      
    speech_files = speech_directory(:, 39:end);  
elseif (odgovor == 4)
    train_music_files = music_directory(:, 1:32);       
    music_files = music_directory(:, 33:end);           

    train_speech_files = speech_directory(:, 1:32);      
    speech_files = speech_directory(:, 33:end);  
else
    disp(['Kriv unos']);
    break;
end
            
        

for i = 1:length(train_music_files)
    train_music_files(i) = strcat('music_speech/music_wav/',train_music_files(i));
    train_speech_files(i) = strcat('music_speech/speech_wav/',train_speech_files(i));
end

for i = 1:length(music_files)
    music_files(i) = strcat('music_speech/music_wav/',music_files(i));
    speech_files(i) = strcat('music_speech/speech_wav/',speech_files(i));
end

% U�enje - pozivanje funkcija rms i zcr

for i = 1:length(train_music_files)
	train_zcr_music(i) = zcr(cell2mat(train_music_files(i)));
	train_rms_music(i) = rms(cell2mat(train_music_files(i)));
end
mean_train_zcr_music = mean(train_zcr_music);
mean_train_rms_music = mean(train_rms_music);

for i = 1:length(train_speech_files)
	train_zcr_speech(i) = zcr(cell2mat(train_speech_files(i)));
	train_rms_speech(i) = rms(cell2mat(train_speech_files(i)));
end
mean_train_zcr_speech = mean(train_zcr_speech);
mean_train_rms_speech = mean(train_rms_speech);

% Testiranje - pozivanje funkcija rms i zcr

for i = 1:length(music_files)
	zcr_music(i) = zcr(cell2mat(music_files(i)));
	rms_music(i) = rms(cell2mat(music_files(i)));
end

for i = 1:length(speech_files)
	zcr_speech(i) = zcr(cell2mat(speech_files(i)));
	rms_speech(i) = rms(cell2mat(speech_files(i)));
end

% Normaliziranje svih dobivenih rezultata na raspon 0-1

min_zcr_music = min([min(train_zcr_music), min(zcr_music)]);
max_zcr_music = max([max(train_zcr_music), max(zcr_music)]);
min_rms_music = min([min(train_rms_music), min(rms_music)]);
max_rms_music = max([max(train_rms_music), max(rms_music)]);

min_zcr_speech = min([min(train_zcr_speech), min(zcr_speech)]);
max_zcr_speech = max([max(train_zcr_speech), max(zcr_speech)]);
min_rms_speech = min([min(train_rms_speech), min(rms_speech)]);
max_rms_speech = max([max(train_rms_speech), max(rms_speech)]);


train_zcr_music = (train_zcr_music - min_zcr_music)/(max_zcr_music - min_zcr_music);
train_rms_music = (train_rms_music - min_rms_music)/(max_rms_music - min_rms_music);
train_zcr_speech = (train_zcr_speech - min_zcr_speech)/(max_zcr_speech - min_zcr_speech);
train_rms_speech = (train_rms_speech - min_rms_speech)/(max_rms_speech - min_rms_speech);

zcr_music = (zcr_music - min_zcr_music)/(max_zcr_music - min_zcr_music);
rms_music = (rms_music - min_rms_music)/(max_rms_music - min_rms_music);
zcr_speech = (zcr_speech - min_zcr_speech)/(max_zcr_speech - min_zcr_speech);
rms_speech = (rms_speech - min_rms_speech)/(max_rms_speech - min_rms_speech);

% Spajanje normaliziranih podataka

music_training = [train_zcr_music; train_rms_music];
speech_training = [train_zcr_speech; train_rms_speech];
music_testing = [zcr_music; rms_music];
speech_testing = [zcr_speech; rms_speech];

% Crtanje grafa
figure;
% Podaci za u�enje:
plot(music_training(1,:), music_training(2,:), '*r');
hold on;
plot(speech_training(1,:), speech_training(2,:), 'go');
% Podaci za testiranje:
plot(music_testing(1,:), music_testing(2,:), '*b');
plot(speech_testing(1,:), speech_testing(2,:), 'ko');

% Imena:
xlabel('ZCR');
ylabel('RMS');
legend('Music (training)','Speech (training)','Music (testing)','Speech (testing)');
grid off;

knn(music_training, music_testing, speech_training, speech_testing);
