# README #
## Classification of sound files to speech or music
### Project for Man-machine communication course

Classification of 128 sound samples, each lasting 30 seconds, done in Matlab.

Used:

* Zero-crossing rate algorithm, 
* Percentage of low-power frames,
* K-nearest neighbour classification