% Knn klasifikacija: (K = 3)

function knn = knn(music_training, music_testing, speech_training, speech_testing)

music_correct = 0;
speech_correct = 0;

% Klasifikacija uzoraka glazbe:
music_classification = [];
for i = 1:length(music_testing)

	% Racuna udaljenost do svake vrijednosti u skupu za ucenje
	for j = 1:length(music_training)
        dist(j) = sum((music_testing(:,i)-music_training(:,j)).^2);
	end

	for j = 1:length(speech_training)
        dist(j+4) = sum((music_testing(:,i)-speech_training(:,j)).^2);
	end

	% Trazi 3 najbliza susjeda
	sort_dist = sort(dist);
	n1 = find(dist == sort_dist(1));
	n2 = find(dist == sort_dist(2));
	n3 = find(dist == sort_dist(3));
	neighbour = [n1,n2,n3];
	neighbour = neighbour(1:3);

	% Ako su >= 2 susjeda glazba klasificiraj kao glazbu
    
	num_music_neighbours = find(neighbour <= 4);
	if (length(num_music_neighbours) >= 2)
		music_classification(i) = 0; 
        music_correct = music_correct + 1;
	else
		music_classification(i) = 1;
	end
end

% Klasifikacija uzoraka govora:
speech_classification = [];
for i = 1:length(speech_testing)

	% Ra�una udaljenost do svake vrijednosti u skupu za u�enje
	for j = 1:length(music_training)
        dist(j) = sum((speech_testing(:,i)-music_training(:,j)).^2);
	end

	for j = 1:length(speech_training)        
        dist(j+4) = sum((speech_testing(:,i)-speech_training(:,j)).^2);
	end

	% Trazi 3 najbliza susjeda
	sort_dist = sort(dist);
	n1 = find(dist == sort_dist(1));
	n2 = find(dist == sort_dist(2));
	n3 = find(dist == sort_dist(3));
	neighbour = [n1,n2,n3];
	neighbour = neighbour(1:3);

	% Ako su >= 2 susjeda govor klasificiraj kao govor
	num_music_neighbours = find(neighbour <= 4);
	if (length(num_music_neighbours) >= 2)
		speech_classification(i) = 0;
	else
		speech_classification(i) = 1;
        speech_correct = speech_correct + 1;
	end
end

% Mjeri se uspjesnost algoritma
correct = (music_correct + speech_correct) / (length(music_classification)+length(speech_classification));

disp(['Glazba (tocno): ', num2str((music_correct/length(music_classification))*100), '% podataka']);
disp(['Govor (tocno): ', num2str((speech_correct/length(speech_classification))*100), '% podataka']);
disp(['Tocno klasificirano ', num2str(correct*100), '% podataka']);
